GIT Basics, 1 day
====

Welcome to this course. Here you will find
* Installation instructions
* Solutions

Installation Instructions
====

You need to have a GIT client installed to perform the exercises and to clone this repo.

* [GIT Client Download](https://git-scm.com/downloads)

Choose the installer for your platform, e.g. Windows 64-bit.
You can go for most of the default settings.
Except for the Windows client;
do not enable the "Git Credential Manager" during the install,
because it currently has a bug and might not work with gitlab.

You also need a simple text editor, such as
* Atom
* Notepad++
* TextPad

Solutions
====
Get the sources by a GIT clone operation

    git clone git@gitlab.com:ribomation-courses/edap/git-basics.git
    cd git-basics

Get the latest updates by a GIT pull operation

    git pull

Links
====
* [GIT](https://git-scm.com/)
* [GitLab](https://about.gitlab.com/)
* [GitHub](https://github.com/)
* [Code Review Tools](https://en.wikipedia.org/wiki/List_of_tools_for_code_review)


***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>
